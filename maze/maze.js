class MazeCell{

	constructor(x, y){
		this.x = x;
		this.y = y;
		this.isVisited = false;
		this.topWall = true;
		this.botWall = true;
		this.leftWall = true;
		this.rightWall = true;
	}	

}
class MazeWall{

	constructor(visited, non, side){
		this.visitedCell = visited;
		this.unvisitedCell = non;
		this.side = side;
	}
}

class Player{

	constructor(){
		this.x = 0
		this.y = 0;
		this.addCircle(0, 0);
	}

	move(direction){
		switch (direction){
			case "left":
				if (!maze[this.x][this.y].leftWall){
					this.removeCircle(this.x, this.y)
					this.x -= 1;
					this.addCircle(this.x, this.y);
				}
				break;
			case "right":
				if (!maze[this.x][this.y].rightWall){
					this.removeCircle(this.x, this.y)
					this.x += 1;
					this.addCircle(this.x, this.y);
				}
				break;
			case "up":
				if (!maze[this.x][this.y].topWall){
					this.removeCircle(this.x, this.y)
					this.y -= 1;
					this.addCircle(this.x, this.y);
				}
				break;
			case "down":
			default:
				if (!maze[this.x][this.y].botWall){
					this.removeCircle(this.x, this.y)
					this.y += 1;
					this.addCircle(this.x, this.y);
				}
				break;
		}
	}

	removeCircle(x, y){
		ctx.beginPath();
		ctx.fillStyle = "#000000";
		ctx.clearRect(wallThickness + (wallThickness + gridSize) * x,
			wallThickness + (wallThickness + gridSize) * y,
			gridSize, gridSize);
	}

	addCircle(x, y){
		ctx.beginPath();
		ctx.fillStyle = "#FF0000";
		ctx.arc((wallThickness + gridSize) * x + wallThickness + gridSize/2, 
			(wallThickness + gridSize) * y + wallThickness + gridSize/2,
			playerSize/2,
			0, Math.PI * 2, 0)
		ctx.fill();
	}
}

var maxRow = 15;
var maxCol = 15;
var gridSize = 20;
var wallThickness = 3;
var playerSize = 16;

var canv = $("#GameCanvas").get(0);
var ctx = canv.getContext("2d");

var maze = [];


drawMaze();
buildMaze();

var player = new Player();

document.onkeydown = movePlayer;

function movePlayer(e){

	e = e || window.event;
	var direction;
	switch (e.keyCode) {
		case 37:
			direction = "left";
			break;
		case 38:
			direction = "up";
			break;
		case 39:
			direction = "right";
			break;
		case 40:
			direction = "down";
			break;
		default:
		return;
	}

	player.move(direction);
}

function buildMaze(){
	for (var i = 0; i < maxCol; i++){
		var col = [];
		for (var j = 0; j < maxRow; j++){
			var cell = new MazeCell(i, j);
			col.push(cell);
		}
		maze.push(col);
	}
	var walls = [];
	addWalls(walls, 0, 0);

	while (walls.length != 0) {
		removeWall(walls);
	}

}

function addWalls(walls, x, y){
	if (x > 0){
		if (! maze[x-1][y].isVisited) walls.push(new MazeWall(maze[x][y], maze[x-1][y], "left"));
	}
	if (y > 0){
		if (! maze[x][y-1].isVisited) walls.push(new MazeWall(maze[x][y], maze[x][y-1], "top"));
	}
	if (x < maxCol-1){
		if (! maze[x+1][y].isVisited) walls.push(new MazeWall(maze[x][y], maze[x+1][y], "right"));
	}
	if (y < maxRow-1){
		if (! maze[x][y+1].isVisited) walls.push(new MazeWall(maze[x][y], maze[x][y+1], "bot"));
	}
	maze[x][y].isVisited = true;
}

function removeWall(walls){
	var wall = walls[Math.floor(Math.random() * walls.length)];
	if (!wall.unvisitedCell.isVisited){
		switch(wall.side) {
			case "left":
				wall.visitedCell.leftWall = false;
				wall.unvisitedCell.rightWall = false;
				break;

			case "right":
				wall.visitedCell.rightWall = false;
				wall.unvisitedCell.leftWall = false;
				break;

			case "top":
				wall.visitedCell.topWall = false;
				wall.unvisitedCell.botWall = false;
				break;

			case "bot":
			default:
				wall.visitedCell.botWall = false;
				wall.unvisitedCell.topWall = false;
				break;
		}
		clearWall(wall.visitedCell, wall.side);
		addWalls(walls, wall.unvisitedCell.x, wall.unvisitedCell.y);
	}

	for (var i = 0; i < walls.length; i++){
		if (walls[i] == wall) walls.splice(i, 1);
	}


}

function drawMaze(){
	canv.width = gridSize * maxCol + wallThickness * (maxCol + 1);
	canv.height = gridSize * maxRow + wallThickness * (maxRow + 1);
	for (var i = 0; i < maxCol + 1; i++){
		ctx.fillRect(i * (gridSize + wallThickness), 0, wallThickness, canv.height);
	}
	for (var j = 0; j < maxRow + 1; j++){
		ctx.fillRect(0, j * (gridSize + wallThickness), canv.width, wallThickness);
	}
}

function clearWall(cell, side){

	var x = (wallThickness + gridSize) * cell.x + wallThickness;
	var y = (wallThickness + gridSize) * cell.y + wallThickness;

	switch(side){
		case "left":
			x -= wallThickness;
			ctx.clearRect(x, y, wallThickness, gridSize);
			break;

		case "right":
			x += gridSize;
			ctx.clearRect(x, y, wallThickness, gridSize);
			break;

		case "top":
			y -= wallThickness;
			ctx.clearRect(x, y, gridSize, wallThickness);
			break;

		case "bot":
		default:
			y += gridSize;
			ctx.clearRect(x, y, gridSize, wallThickness);
	}

}