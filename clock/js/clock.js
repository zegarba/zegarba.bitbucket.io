var currentTime = new Date();

$("#clock").html(setTime());
setInterval(function() {
	tickTime();
	$("#clock").html(setTime())}, 1000);

function tickTime(){
	currentTime.setSeconds(currentTime.getSeconds() + 1);
}

function setTime(){
	var currentHours = currentTime.getHours();
	var currentDay = currentTime.getDay();
	var currentDate = currentTime.getDate();
	var currentMinute = currentTime.getMinutes();
	var currentYear = currentTime.getFullYear();
	var currentMonth = currentTime.getMonth();
	var currentSeconds = currentTime.getSeconds();

	var AM = "AM";

	if (currentHours > 12) {
		AM = "PM";
		currentHours -= 12;
	}
	else if (currentHours == 0) currentHours = 12;

	var month = monthName(currentMonth);
	var day = dayName(currentDay);

	currentMinute = formatTime(currentMinute);
	currentSeconds = formatTime(currentSeconds);

	return "The time is currently: <br> " + currentHours + ":" + currentMinute + ":" + currentSeconds + 
	" " + AM + "<br> " + day + ", " + month + 
	" " + currentDate + ", " + currentYear;
}



function monthName(monthNumber){

	var month;

	switch(monthNumber){
		case 0:
		break;

		case 1:
		month = "February";
		break;

		case 2:
		month = "March";
		break;

		case 3:
		month = "April";
		break;

		case 4:
		month = "May";
		break;

		case 5:
		month = "June";
		break;

		case 6:
		month = "July";
		break;

		case 7:
		month = "August";
		break;

		case 8:
		month = "September";
		break;

		case 9:
		month = "October";
		break;

		case 10:
		month = "November";
		break;

		case 11:
		month = "December";
		break;
	}

	return month;

}

function dayName(dayNumber){
	var day = "";

	switch(dayNumber){

		case 0:
		day = "Sunday";
		break;

		case 1:
		day = "Monday";
		break;

		case 2:
		day = "Tuesday";
		break;

		case 3:
		day = "Wednesday";
		break;

		case 4:
		day = "Thursday";
		break;

		case 5:
		day = "Friday";
		break;

		case 6: 
		day = "Saturday";
		break;
	}
	return day;
}

function formatTime(number){
	if (number < 10){
		return "0" + number;
	}
	else return number;
}

